package vehiculo;




public class Moto extends Vehiculo{
	private String _cilindrada;
	
	public Moto(String marca, String modelo, String cilindrada, Double precio) {
		super._marca= marca;
		super._modelo= modelo;
		super._precio= precio;
		_cilindrada = cilindrada;
	}
	
	@Override
	public String toString() {		
		return "Marca: "+ super._marca +" // " + "Modelo: "+ super._modelo +" // "+
				"Cilindrada: "+ _cilindrada+ " // " + "Precio: $"+ super.cambiarFormatoPrecio();
	}

}
