package vehiculo;


public class Auto extends Vehiculo {
	private int _cantidadPuertas;
	public Auto(String marca, String modelo, int cantPuertas, Double precio) {
		super._marca = marca;
		super._modelo= modelo;
		super._precio= precio;
		_cantidadPuertas = cantPuertas;
		
	}

	@Override
	public String toString() {		
		return "Marca: "+ super._marca +" // " + "Modelo: "+ super._modelo +" // "+
				"Puertas: "+ _cantidadPuertas + " // " + "Precio: $"+ super.cambiarFormatoPrecio();
	}

}
