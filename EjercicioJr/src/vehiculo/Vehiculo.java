package vehiculo;

import java.text.NumberFormat;

public class Vehiculo implements Comparable<Vehiculo> {
	public String _marca;
	public String _modelo;
	public double _precio;
	
	@Override
	public String toString() {
		return "Marca: "+_marca +" // "+ "Modelo: "+ _modelo +" // "+  "Precio: $"+cambiarFormatoPrecio();
		
	}
	
	public String cambiarFormatoPrecio() {
		NumberFormat formatoNumero = NumberFormat.getNumberInstance();
		formatoNumero.setMinimumFractionDigits(2);
		return formatoNumero.format(_precio);
	}

	@Override
	public int compareTo(Vehiculo v) {
		if(_precio > v._precio) {
			return -1;
		}else {
			if(_precio < v._precio) {
				return 1;
			}
		}
		return 0;
	}
	
	

}
