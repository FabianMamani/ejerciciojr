package main;

import Solucion.Solucion;

public class Main {

	public static void main(String[] args) {
		Solucion _solucion = new Solucion();
		_solucion.cargarDatos();
		_solucion.mostrasDetalles();
		imprimirSeparador();
		System.out.println("Veh�culo m�s caro: "+ _solucion.masCaro());
		System.out.println("Veh�culo m�s barato: "+ _solucion.masBarato());
		System.out.println("Veh�culo que contiene en el modelo la letra 'Y':" + _solucion.contieneLetra("Y"));
		imprimirSeparador();
		System.out.println("Veh�culos ordenados por precio de mayor a menor: ");
		_solucion.mostrarEnOrden();
		
	}

	private static void imprimirSeparador() {
		System.out.println("===========================");
	}

}
