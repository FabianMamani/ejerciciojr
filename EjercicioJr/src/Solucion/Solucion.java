package Solucion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import vehiculo.Auto;
import vehiculo.Moto;
import vehiculo.Vehiculo;

public class Solucion {
	ArrayList<Vehiculo> _lista ;
	
	public Solucion() {
		_lista = new ArrayList<Vehiculo>();
	}
	
	public void cargarDatos() {
		_lista.add(new Auto("Peugeot","206",4,200000.00));
		_lista.add(new Moto("Honda","Titan","125c",60000.00));
		_lista.add(new Auto("Peugeot","208",5,250000.00));
		_lista.add(new Moto("Yamaha","YBR","160c",80500.50));
	}
	
	public void mostrasDetalles() {
		for(Vehiculo vehiculo: _lista) {
			System.out.println(vehiculo.toString());
		}
	}
	
	public String masCaro() {
		 return Collections.max(_lista, precio())._marca+" " + Collections.max(_lista, precio())._modelo  ;
	}


	public String masBarato() {
		return Collections.min(_lista,precio())._marca +" "+ Collections.min(_lista,precio())._modelo;
	}

	public ArrayList<Vehiculo> mostrarEnOrden() {
		ArrayList<Vehiculo> ret = _lista;
		Collections.sort(ret);
		imprimirEnOrden(ret);
		return ret;
	}

	private Comparator<Vehiculo> precio() {
		
		return (uno,otro)-> otro.compareTo(uno);
	}

	private void imprimirEnOrden(ArrayList<Vehiculo> _lista) {
		for(Vehiculo v: _lista) {
			System.out.println(v._marca +" "+v._modelo);
		}
	}

	public String contieneLetra(String c) {
		for(Vehiculo v : _lista) {
			if(v._modelo.contains(c)) {
				return (v._marca +" "+ v._modelo+" "+ "$"+v.cambiarFormatoPrecio());
			}
		}
		return null;
	}


}
